\version "2.19.36"
\language "nederlands"

\header {
  title = "Chacony"
  instrument = "Contrabas"
  composer = "Henry Purcell"
  arranger = "Z730"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \minor
  \numericTimeSignature
  \time 3/4
  \tempo 4=65
}

contrabass = \relative c {
  \global
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  % Music follows here.
  g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  \break
  g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  g'2 g4
  \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.

  g'2 g4
  \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.

  g'2 g4
  \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  g'4 f es
  \break
  d2 d4
  es4 f f,
  bes2 bes4
  g2.
  c2 c'4 ~
  c2 c4
  b!2.
  bes2.
  a2.
  \break
  g4 g,2
  a2.
  d2.
  g2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  \break
  d2.
  g,2.
  R1*3/4*8
  g'8 fis g a bes g |
  fis e! fis g a fis |
  g f es d es c |
  \break
  d es d c d g, |
  b! c b! a b! g |
  c b! c d es c |
  d es d c bes a |
  g2.
  \break
  g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  \break
  R1*3/4*8
    g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  \break
  d2.
  g,2.
    g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  \break
  g,2 g'4
  a2.
  bes4 es,2
  f2.
  g2.
  as2.
  g2 g4
  f2.
  g4 g,2
  \break
  c8 d c bes c a |
  d es d c d bes |
  es f es d es c |
  f es f g a f |
  bes a bes c bes a |
  g2 g4
  \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  g'2 g4
  \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
    g'2 g4
    \break
  fis4 fis4. fis8
  g4 es2
  d4 d4.\breathe d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.
  g'2 g4
  fis4 fis4. fis8
  g4 es2
  d4\breathe d4. d8
  b!4 b4. b8
  c4 c2
  d2.
  g,2.\fermata
  \bar "|."


}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
%  \midi { }
}

% engraved date:
date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

% place, then, the following at the end of all scores:
\markup {
  \teeny
  \date
}