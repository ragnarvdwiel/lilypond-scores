\version "2.19.35"
\language "nederlands"

\header {
  title = "Just Squeeze Me"
  subtitle = "(But Don‘t Tease Me)"
  instrument = "Bass"
  composer = "(music) Duke Ellington"
  arranger = "(arr) Roger Pemberton"
  poet = "Lee Gaines (words)"
  meter = "Mod."
  copyright = "1946 Robbins Music Corp"
  % Remove default LilyPond tagline
  tagline = ##f
} 

\paper {
  #(set-paper-size "a4")
  ragged-bottom = ##t
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=96
  \override MultiMeasureRest #'expand-limit = 1
  \compressFullBarRests
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
}

repeatsequence = {f,8. d'16 c8. d16 c,8. d'16 c8. d16}
repeatseqsharp = {f,8. d'16 c8. d16 fis,8. d'16 c8. d16}

contrabass = \relative c {
  \global

  % Music follows here.
  \bar "[|:"
  \repeat volta 2 {
    g2^\markup{\tiny "Gm:7(b5)"}^\markup{\box "play 3xs"} \tuplet 3/2 {r8 g' f} des4 | % 1
    r2^\markup{\fontCaps \sans "F"} \tuplet 3/2 {r8 a' g} \tuplet 3/2 {g c, f,}
  } 
  R1*2
  \repeat volta 2 {
   \repeat percent 3 { f'8. d'16 c8. d16 c,8. d'16 c8. d16 }
   \repeatseqsharp %f,8. d'16 c8. d16 fis,8. d'16 c8. d16
   \repeat percent 2 { g,8. d'16 c8. d16 c,8. d'16 c8. d16 }
   \repeatsequence  %f,8. d'16 c8. d16  c,8. d'16 c8. d16
  } 
  \alternative {
    { \repeatsequence }
    { \repeatsequence }
  }

  f,4 g a bes
  c d es f
  bes,8. bes16 a4 g f
  bes,8 bes'4.-> a4\marcato aes\marcato
  g4 a! b! c
  d e f g^\markup{"(g)"} |
  g8. g16 f4 d bes
  c8\staccato c4\marcato g8\staccato r d4.->
  \repeat percent 3 { f8. d'16 c8. d16 c,8. d'16 c8. d16 }
  \repeatseqsharp      %  f,8. d'16 c8. d16 fis,8. d'16 c8. d16
  \repeat percent 2 { \repeatsequence } %f,8. d'16 c8. d16 c,8. d'16 c8. d16 }
  f,8. d'16 c8. d16 c,8. d'16 c8. d16^\markup{\box \right-align "to coda " \musicglyph #"scripts.coda" }
%  f,8. d'16 c8. d16 c,8. d'16 c8. d16
  \repeatsequence
  \repeat volta 2 { f4^\markup{\box "open for jazz solos"} e d c
                    bes a g c
                    f,4 f g g
                    a a d fis,
                    g a bes b!
                    c c, d e
                    f4 g a c
  }
  \alternative {
    { g'4^\markup{"g"} des c e }
    { f4^\markup{"f"} e d c }
  }
  f,4 g a bes
  c d es f |
  bes, a g f |
  bes,8 bes'4.-> a4 as |
  g4 a b! c
  d4 e f g
  g, a bes b!
  c4 bes a g
  f'4 e d c
  bes4 a g c
  f,4 f g g
  a4 a d fis,
  g4 a bes b!
  c4 c, d e
  f4 g a c
  f4 e d c^\markup{\box \right-align "after all jazz solos D.C. al Coda"}
\bar ":|][|:"
  \repeat volta 2 {
    \once \override Score.RehearsalMark.font-size = #5
 %   \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 1.75 )
    \mark \markup { \musicglyph #"scripts.coda" }
%    f,8. d'16 c8. d16 fis,8. d'16 c8. d16
      \repeatseqsharp
      \repeat percent 2 { \repeatsequence }
      \repeatsequence
  }
  c8. (b!16 bes8) a8 r f4\fermata
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout {
    \context {
      \Score
        %% Changing the defaults from engraver-init.ly
    %defaultBarType = #"!"
    startRepeatType = #"[|:"
    endRepeatType = #":|]"
    doubleRepeatType = #":|][|:"
    }
  }
  \midi { }
}
