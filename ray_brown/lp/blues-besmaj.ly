\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues (I-III)"
  subtitle = "in bes major"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
%  \tempo 4=100
}

contrabassI = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  bes4 f' bes, f' |
  bes,4 f' bes, f' |
  bes,4 f' bes, f' |
  bes,4 f' bes, f' |
  % \break
  es8. bes16 bes4 c d |
  es4 g f8. c16 a4 |
  bes4 f' bes d, |
  g8. d16 bes4 d g |
  % \break
  c,4 g c e,! |
  f8. c'16 f4 g a |
  bes4 a g f |
  es4 d c f |
  \bar "||"
}
contrabassII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % \break
  bes4 d g f |
  es4 g c a |
  bes4 g as f |
  g4 es f d |
  % \break
  es4 g, bes es |
  g bes es e! |
  f4 bes, c es |
  d c b!8. d,16 g4 |
  % \break
  c,4 es g bes |
  a4 \times 2/3 {c,8 es g} f4 a |
  bes8. d16 d,4 es e! |
  f4 g a c
  \bar "||"

}

contrabassIII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  d4 f bes as |
  g4 bes es^\markup{"E"} e! |
  f4^\markup{"f"} es c cis |
  d4 bes as e! |
  % \break
  es4 bes es g'^\markup{"g"} |
  e!4 des bes g |
  f4 bes, g' c, |
  a'4 des,8. c16 bes4 g |
  % \break
  c4 d es e! |
  f4 fis g a |
  bes8. es,16 e4 d des |
  c4 ges'8. e!16 f8. c16 f,4 |
  \bar "||"
}

contrabassIV = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here
  \time 2/2
  bes4 d g f |
  es4 f des d! |
  bes4 d g f |
  bes4 a as d, |
  es4 as g c |
  bes4 g es e! |
  % \break
  f4 bes, f' es |
  d4 as' g des
  c4 g' es d
  c4 ges' f a |
  bes4 d, es e! |
  f4. bes,8 (bes4) r |
  \bar "|."
}

contrabassV = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  \time 2/2
  \partial 8*3 r8 r4 |
  \bar "||"
  bes'4 bes as as |
  g4 g ges ges |
  f4 f es es |
  d4 c bes e! |
  es4 es des des |
  c4 c b! b! |
  % \break
  bes4 bes a a |
  as as g g |
  c4 d es e! |
  f fis g a |
  bes8 d,4. es4 e! |
%  f4 fis g a |
%  bes8 d4. es4 e! |
  f4 g8 bes, (bes4) r |
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassI }
  \layout { }
  %\midi { }
  \header {
    piece = \markup{\bold "Blues (I)"}
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassII }
  \layout { }
    \header {
    piece = \markup{\bold "Blues (II)"}
  }
 % \midi { }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassIII }
  \layout { }
 % \midi { }
   \header {
    piece = \markup{\bold "Blues (III)"}
  }
}

%\markup{\bold \large \box "Jazz Studio"}


\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassIV }
  \layout { }
 % \midi { }
   \header {
     piece = \markup{\bold "Blues (25) jazz studio"}
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassV }
  \layout { }
 % \midi { }
   \header {
    piece = \markup{\bold "Blues (26) jazz studio"}
  }
}
