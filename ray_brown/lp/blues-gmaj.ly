\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues (I-III)"
  subtitle = "in g major"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
%  \tempo 4=100
}

contrabassI = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  %%   \override NoteHead.color = #(x11-color "LimeGreen")
  g4 g g b8. d16 |
  c4 c c e8. g16 |
  g4 a f! g |
  e4 f! d g |
  \break
  c,4 e, g d' |
  c4 a d a8. d16 |
  g4 g c, c |
  b4 b e e, |
  \break
  a4 e' c a |
  d4 c a d |
  g4 d' b e, |
  a4 a, d d |
  \bar "||"
} 

contrabassII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet

  \bar "||"
}

contrabassIII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet

  \bar "||"
}


\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassI }
  \layout { }
  %\midi { }
  \header {
    piece = \markup{\bold "Blues (I)"}
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassII }
  \layout { }
    \header {
    piece = \markup{\bold "Blues (II)"}
  }
 % \midi { }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassIII }
  \layout { }
 % \midi { }
   \header {
    piece = \markup{\bold "Blues (III)"}
  }
}
