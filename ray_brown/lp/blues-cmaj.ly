\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues (I-III)"
  subtitle = "in c major"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
%  \tempo 4=100
}

contrabassI = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  c4 g' c, g'8. e16 |
  f4 c f g |
  c, g a b |
  c c d e |
  \break
  f c f c |
  f a8. a,16 d4 g |
  c, e, g a |
  c g' e c8. a16 |
  \break
  d4 a' f d |
  g d b g |
  c g' e c |
  f d g g, |
  \bar "||"
}
contrabassII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  \break
%  \key f \major
  c'4 bes a g |
  f4 d g, bes8. d16 |
  c4 e, f fis |
  g4 a bes c |
  \break
  f a, ais b |
  c f fis a8. fis16 |
  g4 f e d |
  des bes' \times 2/3 {a8 g e} a,4 |
  \break
  d4 a' f \times 2/3 {d8 f a} |
  g4 a, b d |
  c8 e,4. a4 g |
  f8. f16 fis4 g g' |
  \bar "||"
%  \key f \major
 % f'4 c a f |
 % \bar "||"
 % \break
 % \key bes \major
 % bes4 f bes f |
 % \bar "||"
 % \break
 % \key g \major
 % gis4 gis gis b8. d16 |
}

contrabassIII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  c4 cis d e |
  f c8. f16 fis4 c8. fis16 |
  g4 a bes g |
  e4 f g \times 2/3 {a8 g c,} |
  \break
  a4 d g \times 2/3 {c8 d e} |
  f8.^\markup{"f"} f,16 e,4 f8. fis16 g4 |
  c4 a' d, b' |
  e,4 f8. c16 cis4 a' |
  \break
  d,4 a' f8. a,16 d4 |
  b4 c d g |
  e4 bes'8. gis16 a4 a, |
  d4 as'8. fis16 g4 g, |
  \bar "||"
}


\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassI }
  \layout { }
  %\midi { }
  \header {
    piece = \markup{\bold "Blues (I)"}
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassII }
  \layout { }
    \header {
    piece = \markup{\bold "Blues (II)"}
  }
 % \midi { }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassIII }
  \layout { }
 % \midi { }
   \header {
    piece = \markup{\bold "Blues (III)"}
  }
}
