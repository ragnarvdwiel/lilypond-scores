\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues (II)"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
%  \tempo 4=100
}

contrabass = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.



}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
