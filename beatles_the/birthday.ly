\version "2.19.36"
\language "nederlands"

\header {
  title = "Birthday"
  instrument = "Bass"
  composer = "Paul McCartney"
  meter = "140"
  copyright = "The Beatles"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=140
  \clef bass
}

gstart = {a,8 a, cis e g fis4  e8}

cstart = {c8 c fis a c' b4 a8 }

estart = {e8 e gis b d' cis'4 b8 }

dstart = { d8 d fis a c' b4 a8 }

erep = { e8 e e e e e e e }
electricBass = \absolute {
  \global
  % Music follows here.
  \partial 2 r2 |
  \repeat unfold 4 { \gstart }
  \repeat unfold 2 { \cstart }
  \repeat unfold 4 { \gstart }
  \repeat unfold 2 { \estart }
  \repeat unfold 6 { \gstart }
  \repeat unfold 2 { \dstart }
  \repeat unfold 2 { \gstart }
  \repeat unfold 2 { \estart }
  \repeat unfold 2 { \gstart }
  g8 r r4 r2 |
  R 1*7 |
  \repeat unfold 7 { \erep }
  e8 e e e e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |


}

\score {
  \new Staff \with {
    %midiInstrument = "electric bass (finger)"
    %instrumentName = "Electric bass"
    %shortInstrumentName = "Electric bass"
  } { \clef "bass_8" \electricBass }
  \layout { }
%  \midi { }
}
