\version "2.19.36"
\language "nederlands"

\header {
  title = "Andante festivo"
  subtitle = "Op. 117a"
  instrument = "Contrabasso"
  composer = "Jean Sibelius"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
     ragged-last-bottom = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \major
  \time 2/2
  \tempo 4=60
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c {
  \global
  % Music follows here.
  R1*3
  r2 r4 g4\p (
  g2) g4 a
  b2 b4. (e8)
  e1
  R1*3
  r2 r4 g,4\p (
  g2) g4 a
  b2 b4. (e8)
  e1\<
  d1\!\mp\mark \default
  g,1\> (
  g1)
  R1*3\!
  r2 d'2\<
  g,1\!(\>
  g1)\!
  R1*3
  r2 d'2\<\mark \default g,4\f_\markup{\bold \italic "assai"} g'2 g,4 (
  g4) g'2 g4 (
  g4) g2 g4 (
  g4) g2 g4 (
  g4) g2 g4 (
  g4) g2 g4 (
  g4) g2 g4 (
  g4\<) d4 g, a\!
  b4 c d c
  b4 c c a
  g4\tenuto (b\tenuto) b4.\> (e8\!)
  e2\< d\!\mark \default
  g,1\f
  d'4.( e8) e2
  c4 b a\cresc d\!
  b4 e c f!
  bes,2\ff bes\ff
  a1\ff\>(
  a1\!)
  R1
  g2.\mf g'4
  fis4.( f!8 ) f4 e
  d2\< dis4.( e8)
  e2 d\!\mark \default
  g,4\f_\markup{\bold \italic "assai"} g'2 g,4 (
  g4) g'2 g4(
  g4) g2  g4(
  g4) g2  g4(
  g4) g2  g4(
  g4) g2  g4(
  g4\<) g2  g4(
  g4)d g, a\!
  b4\tenuto c\tenuto d\tenuto c\tenuto
  b4 c c a
  g4\> b b4.( e8\!)
  e4\< b\tenuto e\tenuto d!\!\tenuto\mark \default
  g,2 a4 b
  a4.( g8) g2
  e'2_\markup{\italic \bold "meno"} fis4 g
  fis4.( f!8) f4 e
  d2 dis4.( e8)
  e2_\markup{\bold \italic "poco a poco"} c4 b
  a2\cresc b4\! c
  d2 dis4.( e8)
  e1
  c2 b
  a2 b4( c)
  d2 dis
  e2 d!
  c2\f\< b
  a4\tenuto a\tenuto b\tenuto c\tenuto
  d2\tenuto\ff d\tenuto
  g,^\markup{\bold "allarg."} g2\ff
  g1\ff\fermata
  \bar "|."
}
\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup {
  \teeny
  \date
}