\version "2.19.36"
\language "nederlands"

\header {
  title = "Eine Kleine Nachtmuzik"
  subtitle = "(K. 525)"
  instrument = "cello, contrabas"
  composer = "Wolfgang Amadeus Mozart"
%  meter = "Allegro"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  %   system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
   ragged-last-bottom = ##f
   ragged-bottom = ##t

}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
%  \override Score.MetronomeMark.font-size = #0.5
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

scoreAContrabass = \relative c {
  \global
  \key g \major
  \time 4/4
  \tempo "Allegro" 4=126
				% Music follows here.
  % first bit

  \repeat volta 2 {
  g'4\f r8 d g4 r8 d | % 1
  g8 d g b d4 r4 | % 2
  c4 r8 a c4 r8 a | % 3
  c8 a fis a d,4 r | % 4
  \repeat unfold 8 {g8} | % 5

  \break
  \repeat unfold 8 {g8} | % 6
  \repeat unfold 8 {g8} | % 7
  \repeat unfold 8 {g8}| % 8
  g8 g a a b b fis fis | % 9
  g g a a b4 r | % 10
  r1 | % 11
  \break
  d,2\p\upbow (e) | % 12
  c4 c d d | % 13
  b8\staccato r d\staccato r g4 r | % 14
  r1 |
  d2\upbow (d) | % 16
  c4 c d d | % 17
  g8 \sf g g g g\p g g g | % 18
  \break
  g8\sf g g g g\p g g g | % 19
  g8 g g g g\< g g\! g | % 20
  g8 g fis fis g g e e | % 21
  d1\f | % 22
  d1 | % 23
  d8 e fis e d e fis d | % 24

  \break
  g8 a b a g a b gis | % 25
  \repeat unfold 8 {a8} | % 26
  a8 a, a a a4 r | % 27
  \mark \default r2 r4 dis8\staccato\p r | % 28
  e8\staccato r d!\staccato r cis\staccato r a\staccato r | % 29
  \break
  r4 ais (b8) r g!\staccato r | % 30
  a!4 a'4. (gis8 g! e) | % 31
  d8\staccato r e\staccato r fis\staccato r dis\staccato r | % 32
  e8\staccato r d!\staccato r cis r a\staccato r | % 33
  b8\staccato r g'!\staccato r a\staccato r a,\staccato r | % 34
  \break
  d4 r r2 | % 35
  a4 r r2 | % 36
  d4 r r2 | % 37
  a4 r r2 | % 38
  r8 d8\f fis e dis b cis dis | % 39
  e8 g e d! cis a b cis | % 40
  \break
                      \override NoteHead.color = #(x11-color "DodgerBlue")
  d8 d e fis g g g g | % 41
  a8 a a a a, a a a | % 42
  \mark \default d4 r r2 | % 43
  a4\p r r2 | % 44
  d4 r r2 | % 45
  a4 r r2 | % 46
  \break
  r8 d\f fis e dis b cis dis | % 47
  e8 g e d cis a b cis | % 48
  d8 d e fis g g g g | % 49
  a8 a a a a, a a a | % 50
  \break
  d8 a b cis d d e e | 	% 51
  fis8 cis d e fis fis g g | % 52
  a8 a ais ais b4 r | % 53
  g2\p\upbow (a!) | % 54
  d,8\staccato r d\staccato r d\staccato r r4 | % 55
  }
  \break
  \repeat volta 2 {
    d4\f r8 a d4 r8 a | % 56
    d8 a d fis a4 r | % 57
    a4 r8 fis a4 r8 fis | % 58
    a8 fis dis fis b,4 r | % 59
    c4\p r r2 | % 60
    a4 r r2 | % 61
    c4 r r2 | % 62
    \break
    g4 r r2 | % 63
    c4 r r2 | % 64
    e4 r r2 | % 65
    a,4 r r2 | % 66
    d4 r r2 | % 67
    es1\upbow ( | % 68
    e1) | % 69
    d4 r8 d\f (e!8 fis g a  | % 70
    c8 bes) r fis (g a bes cis | % 71
    \break
    e!8 d) r4 r2 | % 72
    R1*4/4*2 | % 73-74
    d,1\p | % 75
    \mark \default g4\f r8 d g4 r8 d | % 76
    g8 d g b d4 r | % 77
    c4 r8 a c4 r8 a | % 78
    c8 a fis a d4 r | % 79
    g,2:8 g:8 | % 80
    \break
    g2:8 g2:8 | % 81
    g2:8 g2:8 | % 82
    g2:8 g2:8 | % 83
    g8 g a a b b fis fis | % 84
    g8 g a a b4 r | % 85
    R1 | % 86
    d,2\p (e) | % 87
    c4 c d d | % 88
    \break
    b8 r d r g4 r | % 89
    R1 | % 90
    d2\upbow (e) | % 91
    c4 c d d | % 92
    g2:8\sf g2:8\p | % 93
    g2:8\sf g2:8\p | % 94
    g2:8 g2:8\cresc | % 95
    g8\! g fis fis g g e e | % 96
    d1\f | % 97
    d1 | % 98
    \break
    d2:8  d2:8 | % 99
    d2:8  d4 r | % 100
    \mark \default r2 r4 gis8\staccato\p r | % 101
    a8\staccato r g\staccato r fis\staccato r d\staccato r | % 102
    r4 dis4 (e8) r c\staccato r | % 103
    d4 b'4. (cis8 c! a) | % 104
    \break
    g8\staccato^\markup{\bold \box "check"} r a\staccato r b\staccato r gis\staccato r | % 105
    a8\staccato r g\staccato  r fis\staccato  r d\staccato  r |
    e8\staccato  r c\staccato  r d\staccato  r d\staccato  r |
    a4 r r2 |
    d4 r r2 |
    a4 r r2 |
    d4 r r2^\markup{"111"} |
    \break
    r8 g\f b a gis e fis g | % 112
    a8 c a g fis d e fis | % 113
    g8\staccato g\staccato a\staccato b\staccato c c c c | % 114
    d8 d d d d, d d d | % 115
    g4 r r2 | % 116
    \break
    d4\p r r2 | % 117
    g4 r r2 | % 118
    d4 r r2 | % 119
    r8 g\f b a gis e fis g | % 120
    a8 c a g fis d e fis | % 121
    g8\staccato g\staccato a\staccato b\staccato c2:8 | % 122
    d2:8 d,:8 | % 123
    \mark \default g8 d e fis g g a a | % 124
    b8 fis g a b b c c | % 125
    d8 d dis dis e4 r | % 126
    c,2\p (d) | % 127
    g,4 r r2 | % 128
    c2 (d) | % 129
    g,4 r r2 | % 130
    \break
    c4 r d\f r | % 131
    g,2:8 g2:8 | % 132
    g2:8 g2:8 | % 133
    g2:8 g2:8 | % 134
    g2:8 g2:8 | % 135
    g8 b d g b g d' b | % 136
    g4 g,8.\tenuto (g16) g4 r | % 137
    
  }
  
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreAContrabass }
  \layout { }
  \header {
   piece =   \markup{\large \bold "Ie movement"}
  }
%  \midi { }
}

\pageBreak
scoreBContrabass = \relative c {
  \global
  \key c \major
  \tempo "Andante" 4=80
  \time 2/2
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
				% Music follows here.
  \repeat volta 2 {
    \partial 2 r2 | % 1
    c4\p c c c | %  2
    c c r c | % 3
    r4 c (b  c) | % 4
    g'4 g, r2 | % 5
    c4\f c c c | % 6
    c4 c e e | % 7
    f4 f g g, | % 8
    \break
    c4 r
  }
  \repeat volta 2 {
    \partial 2 r2 | % 9
    \set Score.currentBarNumber = #8
    g4\p g g g | % 10
    g4 r r2 | % 11
    g4\< g'\! g g | % 12
    g,4\f r r2 | % 13
    a2\p (d4 b) | % 14
    c4 r e\f d | % 15
    c4 f g g, | % 16
        \set Score.currentBarNumber = #15
     c4 r
    \break
  }
      \override NoteHead.color = #(x11-color "DodgerBlue")
      \set Score.currentBarNumber = #16
  \repeat volta 2 {

    c'8\staccato\p (c\staccato c\staccato c\staccato) | % 16b
    g4 r c,8\staccato (c\staccato c\staccato c\staccato) | % 17
    g4 r c'8\staccato (c\staccato a\staccato a\staccato) | % 18
    g4 r8 e8 d4 r8 d | % 19
    \partial 2 g4 (g8) r | % 20
  }
  \repeat volta 2 {
    \partial 2 e8\staccato (e\staccato g\staccato g\staccato) | % 21
    \break
    \set Score.currentBarNumber = #21
    a4 r d,8\staccato (d\staccato f\staccato f\staccato) | % 21b
    g4 r c8\staccato (c\staccato a\staccato a\staccato) | % 22
    gis4 r8 e8 (fis4) r8 fis ( | % 23
    e4) r8 gis8 (a) a\staccato a\staccato a\staccato | % 24
    gis4 r8 e8 (fis4) r8 fis ( | % 25
    e4) r8 fis8 (e4) r | % 26
    \break
    r2 r4 r8 a, | % 27
    gis8\staccato (b) e,\staccato r r2 | % 28
    r2 r4 r8 c'\staccato | % 29
    b8 (d) g,!\staccato  b\staccato \mark \default c4 r | % 30
    c4 c c c | % 31
    c4 c r c | % 32
    r4 c (b c) | % 33
    \break
    g'4 g, r2 | % 34
    c4\f c c c | % 35
    c4 c e e | % 36
    fis4 fis g g, | % 37
    \partial 2 c4 r
  }
  \repeat volta 2 {
    \key es \major
    \set Score.currentBarNumber = #38
    \partial 2 r2
    r4 g'8\p\upbow\staccato b,!\turn (c) r r4 | % 39
    r4 g'8\p\upbow\staccato b,!\turn (c) r r4 | % 40
    \break
    bes4.\fp (c8 as) a\staccato bes\staccato bes\staccato | % 41
    \partial 2 e,8 r r4
  }
  \partial 2 r2
    r4 c''8\upbow e,!\turn (f) r r4 | % 44
    r2 r4 c8\staccato fis\turn ( | % 45
    g8) aes  g f! ( es) b!\turn (c) as\turn ( | % 46
    g8) r8 r4 g'8\staccato b,!\turn (c) r | % 47
    g'8 b,!\turn c r g' b,!\turn (c) r % 48
    R1
    g4.\downbow (a!16 b!) \bar "||" \mark \default \key c \major c8 r r4 | % 
  \break
  g4\p g g g | % 59?
  g4 r r2 | % 60?
  g4\< g'\! g g | %
  g,4\f r \mark \default r2 | %
  a2\p (d4 b) | %
  c4 r e\f d | %
  c4 f g g, | %
  \break
  c4 r e,\f r | %
  f4 r fis r | %
  g1\p ( | %
  c4) r r2 | %
  g1 ( | %
  c4) r r2 | %
  g1 ( | %
  c8) r g r d' r r4 | %
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreBContrabass }
  \layout {
  }
  \header {
    piece = \markup{\large \bold "II. Romanze"}
  }
%  \midi { }
}

scoreCContrabass = \relative c {
  \global
    \key g \major
  \tempo "Allegretto" 4=105
  \time 3/4
  % Music follows here.
				% menuetto
%      \override NoteHead.color = #(x11-color "DodgerBlue")
  \repeat volta 2 {
    \partial 4 r4
        \set Score.currentBarNumber = #1
    b'4\staccato\f fis\staccato g\staccato | % 2
    a4\staccato e\staccato fis\staccato | % 3
    g4\staccato e\staccato cis\staccato | % 4
    d4\staccato d'\staccato c!\staccato | % 5
    b4\staccato fis\staccato g\staccato | % 6
    a\staccato d,\staccato g\staccato | % 7
    e\staccato c\staccato d\staccato | % 8
    g,4\staccato g'\staccato
  }
  \break
  \repeat volta 2 {
    \partial 4 r4 % 9?!
    r4 r a\staccato\p | % 10
    g4 (e) r | %
    r4 r g\staccato | %
    fis4\staccato\< d'\staccato\! c\staccato | %
    b4\staccato\f a\staccato g\staccato | %
    fis4\staccato d\staccato g\staccato | %
    e4\staccato c\staccato d\staccato | %
    \partial 2 g,4\staccato g'\staccato_\markup{\italic \bold "Fine"} | %
  }
  \break
  \repeat volta 2 {
    \key d \major
    \partial 4 r4^\markup {\bold "Trio"} | % 17
    d\staccato\p d\staccato r | %
    d4\staccato d\staccato r | %
    a4\staccato a'\staccato r | %
    a,4\staccato a'\staccato r | %
    d,4\staccato d'\staccato r | %
    b,4\staccato b'\staccato r | %
    g4\staccato e\staccato a\staccato | %
    \partial 2 d,4\staccato d'\staccato
  }
  \repeat volta 2 {
    \partial 4 r4 | %
    g,,4\f r cis ( | %
    d4) r r | %
    \break
    e4 r e, | % 27?
    a4\staccato a'\staccato r | %
    d,4\staccato\p d\staccato r | %
    d4\staccato d\staccato r | %
    a4\staccato a'\staccato r | %
    a,4\staccato a'\staccato r | %
    d,4\staccato d'\staccato r | %
    b,4\staccato b'\staccato r | %
    g4\staccato_\markup{\smaller \italic \bold "Menuetto da capo"} e\staccato a\staccato | %
    \partial 2 d,4\staccato d'\staccato  |%
  }
}


\pageBreak

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreCContrabass }
  \layout { }
  \header {
    piece = \markup{\large \bold "III. Menuetto"}
  }
%  \midi { }
}



scoreDContrabass = \relative c {
  \global
  \key g \major
  \tempo "Allegro" 4=126
  \time 2/2
  % Music follows here.
				% Rondo

  \partial 2 r2 | % 1
  \repeat volta 2 {
    \set Score.currentBarNumber = #1
    g'2\p r | % 1
    d4\staccato r g\staccato r | % 2 
    fis4\staccato r g\staccato r | % 3
    d4\staccato r r2 | % 4
    g2 r | % 5
    d4\staccato r e\staccato r | % 6
    c4\staccato r d\staccato r | % 7
    \break
  }
  \alternative {
    {g4\staccato g\staccato b,\staccato d\staccato}
    {g,8\f g' g g g2:8}
  }
  g,8 g' g g g2:8 | % 9
  g2 (g8) (a) fis (d) | % 10
  b'2 (b8) (c) a (fis) | % 11
  g4 b8\staccato\downbow b\staccato b (g) g\staccato g\staccato | % 12
        \override NoteHead.color = #(x11-color "DodgerBlue")
  \break
      \set Score.currentBarNumber = #13
  g8 ( d) d\staccato d\staccato d\staccato (b) b (g) | % 13
}

%\pageBreak

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreDContrabass }
  \layout { }
  \header {
    piece = \markup{\large \bold "IV. Rondo"}
  }
%  \midi { }
}


\markup {
  \teeny
  \date
}


% Eine Kleine Nachtmusik (Serenade No. 13 for strings in G Major),
% K. 525, is a 1787 composition for a chamber ensemble by Wolfgang
% Amadeus Mozart. The German title means “a little serenade”, though it
% is often rendered more literally but less accurately as “a little
% night music”. The work is written for an ensemble of two violins,
% viola, and cello with optional double bass, but is often performed by
% string orchestras.

% The serenade was completed in Vienna on 10 August 1787, around the
% time Mozart was working on the second act of his opera Don
% Giovanni. It is not known why it was composed. Hildesheimer (1991,
% 215), noting that most of Mozart's serenades were written on
% commission, suggests that this serenade, too, was a commission, whose
% origin and first performance were not recorded.

% The traditionally used name of the work comes from the entry Mozart
% made for it in his personal catalog, which begins, “Eine kleine
% Nacht-Musik”. As Zaslaw and Cowdery point out, Mozart almost certainly
% was not giving the piece a special title, but only entering in his
% records that he had completed a little serenade, in German, a
% “Ständchen”.

% The work was not published until about 1827, long after Mozart's
% death, by Johann André in Offenbach am Main. It had been sold to this
% publisher in 1799 by Mozart's widow Constanze, part of a large bundle
% of her husband's compositions.

% Today the serenade is widely performed and recorded; indeed both
% Jacobson (2003, 38) and Hildesheimer (1992, 215) opine that the
% serenade is the most popular of all Mozart's works. Of the music,
% Hildesheimer writes, “even if we hear it on every street corner, its
% high quality is undisputed, an occasional piece from a light but happy
% pen.”

% The first movement is in sonata-allegro form. It opens with an
% ascending Mannheim rocket theme. The second theme is more graceful and
% in D major, the dominant key of G major. The exposition closes in D
% major and is repeated. The development section begins on D major and
% touches on D minor and C major before the work returns to G major for
% the recapitulation.
